#!/bin/bash

docker stop prod-app
docker rm prod-app

docker image rm prod-app-image

docker build -t prod-app-image .

docker run -it -d -p 9000:3000 -v $(pwd):/app --name prod-app prod-app-image


