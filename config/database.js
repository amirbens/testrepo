const Sequelize = require('sequelize');
const env = process.env;
const connect =  ()=>{
    sequelize = new Sequelize(env.DB,env.USER,env.PASSWORD,{
        host: env.ENDPOINT,
        port: env.PORT,
        dialect: 'mysql',
        define: {
          timestamps: false
        },
        pool: {
          max: 10,
          min: 0,
          acquire: 30000,
          idle: 10000
        },
        dialectOptions: {
          useUTC: false, //for reading from database
          dateStrings: true,
          typeCast: function (field, next) { // for reading from database
            if (field.type === 'DATETIME') {
              return field.string()
            }
            return next()
          },
        },
        timezone: '-04:00'
      });
      
      console.log(`host: ${env.ENDPOINT} - db: ${env.DB}`);
      return sequelize;
}
module.exports =connect;

