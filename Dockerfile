FROM node:9-slim
WORKDIR /app
COPY package.json /app
RUN npm install pm2 -g
RUN npm install
COPY . /app
CMD ["pm2-runtime","index.js","--watch"]
